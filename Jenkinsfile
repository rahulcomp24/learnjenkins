def app

pipeline {
    agent any

    environment {
        registry = 'rahulcomp24/learnjenkins'
        registryCredential = 'gitlab-access-token'
    }

    stages {
        stage('Lint') {
            steps {
                echo 'Building..'
            }
        }
        stage('Test') {
            steps {
                echo 'Testing..'
            }
        }
        stage('Build') {
            steps {
                script {
                    app = docker.build(registry)
                }
            }
        }
        stage('Deploy dev') {
            steps {
                script {
                    docker.withRegistry('https://registry.gitlab.com', registryCredential ) {
                        app.push(env.BUILD_ID)
                        app.push('dev')
                    }
                }
            }
        }
        stage('Deploy prod') {
            when {
                expression {
                    return env.GIT_BRANCH == 'origin/main'
                }
            }
            steps {
                script {
                    docker.withRegistry('https://registry.gitlab.com', registryCredential ) {
                        app.push('prod')
                    }
                }
            }
        }
    }

    post {
        always {
            emailext body: "${currentBuild.currentResult}: Job ${env.JOB_NAME} build ${env.BUILD_NUMBER}\n More info at: ${env.BUILD_URL}",
            recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']],
            subject: "Jenkins Build ${currentBuild.currentResult}: Job ${env.JOB_NAME}"
        }
    }
}
